/** 
 * @name ExtJS rich editor component 
 * @author crazymus < QQ:291445576 >
 * @version 1.0.0
 * @licenced Apache2.0
 * @updated 2015-09-05
 */
 
Ext.define('ExtDash.view.ExtEditor',{
	extend:'Ext.Component',
	xtype:'ExtEditor',
	fieldLabel:'ExtEditor编辑器',
	id:'', //unique id,required
	width:800,
	height:300,
	editorConfig:[
		'source','fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
		'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
		'insertunorderedlist', '|', 'emoticons', 'image', 'link','|','fullscreen','about'
	],
	listeners:{
		'render':function(){
			var me = this,
				id = 'ext_editor_'+me.id;
			me.setHtml([
				'<p style="margin:0 0 5px 0;">'+me.fieldLabel+'</p>',
				'<textarea id="'+id+'" style="width:'+
				me.width+'px;height:'+
				me.height+'px;"></textarea>'
			].join(''));
			//初始化编辑器
			me.editor = KindEditor.create('#'+id,{
				items:me.editorConfig
			});
		}
	},
	//public
	getValue:function(){
		return this.editor.html();
	},
	//public
	setValue:function(val){
		this.editor.html(val);
	}
	
});