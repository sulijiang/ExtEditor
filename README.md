####ExtEditor
ExtJS富文本编辑器插件，基于KindEditor。
ExtJS自带的富文本编辑器功能有限，局限性很大。本插件整合当前流行的KindEditor框架，使用简便，功能强大。

####快速上手
引入KindEditor类库：
```html
<script src="kindeditor/kindeditor.js"></script>
<script src="kindeditor/lang/zh_CN.js"></script>
```
引入ExtEditor:
```html
<script src="ExtEditor.js"></script>
```
使用组件：
```javascript
var win = Ext.widget('window',{
	items:[
		{
			xtype:'ExtEditor',
			id:'article',//唯一标识，建议指定
			fieldLabel:'内容', //label
			width:800, //宽度
			height:250 //宽度
		}
	],
	bodyPadding:10,
	width:900,
	height:400
});

win.show();
```
自定义配置项：
```javascript
{
	xtype:'ExtEditor',
	id:'article',//唯一标识，建议指定
	fieldLabel:'内容', //label
	width:800, //宽度
	height:250, //宽度
	editorConfig:[
		'source','table','music',...,'video'
	] //详细参数请参考KindEditor官网
}
```
设置编辑器内容：
```javascript
var editor = Ext.getCmp('article');//根据id获取组件
	editor.setValue("hello,world!"); 
```
获取编辑器内容：
```javascript
var editor = Ext.getCmp('article'),//根据id获取组件
	val    = editor.getValue(); 
```